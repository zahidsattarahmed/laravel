<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
class TaskController extends Controller
{
    public function index()
    {
        $tasks =Task::all();
        return view('task',compact('tasks'));
    }
    
   public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            
        ]);
  
        Task::create($request->all());
   
        $tasks =Task::all();
        return view('task',compact('tasks'));
    }
   
  
   
    public function edit(Task $task)
    {
       
        return view('edit',compact('task'));
    }
    
    public function update(Request $request, Task $task)
    {
        $request->validate([
            'name' => 'required',
            
        ]);
  
        $task->update($request->all());
  
        $tasks =Task::all();
        return view('task',compact('tasks'));
    }
  
  
    public function destroy(Task $task)
    {
        $task->delete();
  
        return view('task');
    }

}
  







