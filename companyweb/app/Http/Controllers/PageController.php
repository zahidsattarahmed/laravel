<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    public function index(){
        $header=[
            'logo'=>'public/frontend/img/logo.png',
            
            
            
            


        ];
        $menu=
        [
            [
            'link'=>'#hero_section',
            'name'=>'Home'],
            [
                'link'=>'#aboutUs',
                'name'=>'About Us'
            ],
            [
                'link'=>'#service',
                'name'=>'Services'

            ],
            ['link'=>'#Portfolio',
            'name'=>'Portfolio'
           
            ],
            ['link'=>'#clients',
            'name'=>'Clients'
        ],
            [ 'link'=>'#team',
            'name'=>'Team'
        ],
            ['link'=>'#contact',
            'name'=>'contact'
            ]
        ];
        $hero_area=[
            'img'=>'public/frontend/img/main_device_image.png',
            'h1'=>'We create',
            'h2'=>'web templates',
            'st'=>'awesome',
            'p'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text  printer took a galley of type and scrambled it to make a type specimen.',
            'link'=>'#service',
            'name'=>'Read more'

        ];
        $about=[
            'img'=>'public/frontend/img/about-img.jpg',
            'h3'=>'Lorem Ipsum has been the industrys standard dummy text ever..',
            'p1'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.PageMaker including versions of Lorem Ipsum.',
            'p2'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged like Aldus PageMaker including versions of Lorem Ipsum.',
            'sp'=>'Want to know more..',
            'link'=>'#contact',
            'h2'=>'About Us',
            'bt'=>'Contact Us',

        ];
        $service=[
            'name'=>'Services',
            '1'=>'Android',
            '2'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text',
            '3'=>'Apple IOS',
            '4'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text',
            '6'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text',
            '5'=>'Design',
            '7'=>'Concept',
            '8'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text',
            '9'=>'User Research',
            '10'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text',
            '11'=>'User Experience',
            '12'=>'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text',




        ];
        $clients=[
            ['img'=>'public/frontend/img/client_logo5.png',],['img'=>'public/frontend/img/client_logo1.png',],['img'=>'public/frontend/img/client_logo2.png',],['img'=>'public/frontend/img/client_logo3.png',]


        ];
        $social_links=[
            [
            'fb'=>'fa fa-facebook',
            'link'=>'facebook.com',
            'delay'=>'facebook animated bounceIn wow delay-03s'
        ],[
            'fb'=>'fa fa-twitter', 
            'link'=>'twitter.com',
            'delay'=>'twitter animated bounceIn wow delay-02ss'
        ],[
            'fb'=>'fa fa-pinterest',
            'link'=>'pinterest.com',
            'delay'=>'pinterest animated bounceIn wow delay-04s'
        ],[
            'fb'=>'fa fa-google-plus',
            'link'=>'google.com',
            'delay'=>'gplus animated bounceIn wow delay-05s'

            
        ]];
       

        
        return view('header',compact('header','menu','hero_area','about','service','clients','social_links'));
    }
}
