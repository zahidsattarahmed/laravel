@extends('master')
@section('header-content')
<header id="header_wrapper">
  <div class="container">
    <div class="header_box">
        @foreach($header as $headerlogo)
      <div class="logo"><a href="#"><img src="{{url($header['logo'])}}" alt="logo"></a></div>
      @endforeach
	  <nav class="navbar navbar-inverse" role="navigation">
      <div class="navbar-header">
        <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
	    <div id="main-nav" class="collapse navbar-collapse navStyle">
			<ul class="nav navbar-nav" id="mainNav">
              @foreach($menu as $menuItem)
              <li  class="{{ $loop->first ? 'active' : '' }}"><a href="{{$menuItem['link']}}" class="scroll-link">{{$menuItem['name']}}</a></li>
              @endforeach
			 
			</ul>
      </div>
	 </nav>
    </div>
  </div>
</header>
@endsection
@section('hero')
<section id="hero_section" class="top_cont_outer">
  <div class="hero_wrapper">
    <div class="container">
      <div class="hero_section">
        <div class="row">
          <div class="col-lg-5 col-sm-7">
              
            <div class="top_left_cont zoomIn wow animated"> 
                
              <h2>{{$hero_area['h1']}} <strong>{{$hero_area['st']}}</strong> {{$hero_area['h2']}}</h2>
              <p>{{$hero_area['p']}}</p>
              <a href="{{$hero_area['link']}}" class="read_more2">{{$hero_area['name']}}</a> </div>
          </div>
          <div class="col-lg-7 col-sm-5">
			<img src="{{url($hero_area['img'])}}" class="zoomIn wow animated" alt="" />
          </div>
          
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
@section('about')
<section id="aboutUs"><!--Aboutus-->
<div class="inner_wrapper">
  <div class="container">
    <h2>{{$about['h2']}}</h2>
    <div class="inner_section">
	<div class="row">
      <div class=" col-lg-4 col-md-4 col-sm-4 col-xs-12 pull-right"><img src="{{url($about['img'])}}" class="img-circle delay-03s animated wow zoomIn" alt=""></div>
      	<div class=" col-lg-7 col-md-7 col-sm-7 col-xs-12 pull-left">
        	<div class=" delay-01s animated fadeInDown wow animated">
			<h3>{{$about['h3']}}</h3><br/> 
            <p>{{$about['p1']}}</p> <br/>
<p>{{$about['p2']}}</p>
</div>
<div class="work_bottom"> <span>{{$about['sp']}}</span> <a href="$about['link']" class="contact_btn">{{$about['bt']}}</a> </div>       
	   </div>
      	
      </div>
	  
      
    </div>
  </div> 
  </div>
</section>
@endsection
@section('services')
<section  id="service">
  <div class="container">
    <h2>{{$service['name']}}</h2>
    <div class="service_wrapper">
      <div class="row">
        <div class="col-lg-4">
          <div class="service_block">
            <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="fa fa-android"></i></span> </div>
            <h3 class="animated fadeInUp wow">{{$service['1']}}</h3>
            <p class="animated fadeInDown wow">{{$service['2']}}</p>
          </div>
        </div>
        <div class="col-lg-4 borderLeft">			
          <div class="service_block">
            <div class="service_icon icon2  delay-03s animated wow zoomIn"> <span><i class="fa fa-apple"></i></span> </div>
            <h3 class="animated fadeInUp wow">{{$service['3']}}</h3>
            <p class="animated fadeInDown wow">{{$service['4']}}</p>
          </div>
        </div>
        <div class="col-lg-4 borderLeft">
          <div class="service_block">
            <div class="service_icon icon3  delay-03s animated wow zoomIn"> <span><i class="fa fa-html5"></i></span> </div>
            <h3 class="animated fadeInUp wow">{{$service['5']}}</h3>
            <p class="animated fadeInDown wow">{{$service['6']}}</p>
          </div>
        </div>
      </div>
	   <div class="row borderTop">
        <div class="col-lg-4 mrgTop">
          <div class="service_block">
            <div class="service_icon delay-03s animated wow  zoomIn"> <span><i class="fa fa-dropbox"></i></span> </div>
            <h3 class="animated fadeInUp wow">{{$service['7']}}</h3>
            <p class="animated fadeInDown wow">{{$service['8']}}</p>
          </div>
        </div>
        <div class="col-lg-4 borderLeft mrgTop">
          <div class="service_block">
            <div class="service_icon icon2  delay-03s animated wow zoomIn"> <span><i class="fa fa-slack"></i></span> </div>
            <h3 class="animated fadeInUp wow">{{$service['9']}}</h3>
            <p class="animated fadeInDown wow">{{$service['10']}}</p>
          </div>
        </div>
        <div class="col-lg-4 borderLeft mrgTop">
          <div class="service_block">
            <div class="service_icon icon3  delay-03s animated wow zoomIn"> <span><i class="fa fa-users"></i></span> </div>
            <h3 class="animated fadeInUp wow">{{$service['11']}}</h3>
            <p class="animated fadeInDown wow">{{$service['12']}}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection