<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <link rel="stylesheet" href="{{url('public/css/app.css')}}">
    <script src="{{url('public/js/app.js')}}" type="text/javascript"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src=""></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <title>Ajax </title>
</head>
<body>
    <div class="page-header text-center">
        <h1>Project Ajax</h1>
      </div>
    
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <table id="datatable" class="table table-bordered table-striped">
                   <tr>
                       <th>#</th>
                       <th>name</th>
                       <th>detail</th>
                       <th>author</th>
                       <th>action</th>
                   </tr>
                   <tbody id="bodyData">

                </tbody>
                </table>
            </div>
            <div class="col-md-4">
                <form action="">
                   
                    <div class="form-group myid">
                        <label for=""></label>
                        <input type="number" name="" id="id" class="form-control" readonly="readonly" >
                    </div>
                    <div class="form-group">
                        <label for="Name">Name</label>
                        <input type="text" name="name" id="name" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="Detail">Detail</label>
                        <textarea name="detail" id="detail" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="Author">Author</label>
                        <input type="text" name="author" id="author" class="form-control">
                    </div>
                    <button type="button" id="save" onclick="saveData()" class="btn btn-primary">Save</button>
                    <button type="button" id="update" onclick="updateData()" class="btn btn-warning">Update</button>
                </form>
            </div>
            
        </div>
    </div>
    <script type="text/javascript">
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
    $('#save').show();
    $('#update').hide();
    $('.myid').hide();
    function viewData(){
        $.ajax({
          type:"GET",  
          dataType:"json",
          url:"/ajax/create",
          success:function(response){
            console.log(response);
            
              var rows="";
              var i=0;
              $.each(response,function(key,value){
                  i=i+1;
                  rows=rows+"<tr>";
                  rows=rows+"<td>"+i+"</td>";
                  rows=rows+"<td>"+value.name+"</td>";
                  rows=rows+"<td>"+value.detail+"</td>";
                  rows=rows+"<td>"+value.author+"</td>";
                  rows=rows+'<td>';
                    rows=rows+"<button type='button' class='btn btn-warning' onclick='editData("+value.id+")'>Edit</button>"
                    rows=rows+"<button type='button' class='btn btn-danger' onclick='deleteData("+value.id+")'>Delete</button>"
                    rows=rows+'</td>';
                    rows=rows+ "</tr>";
                // rows += '<tr><td>' + value.id + '</td><td>' + value.name + '</td><td>' + value.detail + '</td></tr>';
       
       


                  
              });
              $('#bodyData').html(rows);

          }
        })

    }
    viewData();
    function saveData(){
        var name=$('#name').val();
        var detail=$('#detail').val();
        var author=$('#author').val();
        console.log(name);
       
        $.ajax({
            type:'POST',
            dataType:'json',
           

            data:{name:name,detail:detail,author:author},
            url:"/ajax/create",
            success:function(response){
                console.log(response);
                viewData();
                clearData();
                $('#save').show();
                
            }
        })


    }
    function updateData()
    {
        var id=$('#id').val();
        var name=$('#name').val();
        var detail=$('#detail').val();
        var author=$('#author').val();
        console.log(id);
        console.log(author);
        $.ajax({
            type:'PUT',
            dataType:'json',
            data:{name:name,detail:detail,author:author},
           
            url:"create/"+id,
            success:function(reponse){
                
                viewData();

                clearData();
                $('.myid').hide();
                $('#save').show();
                $('#update').hide();
            }
        })


    }
    function clearData()
    {
        $('#id').val('');
        $('#name').val('');
        $('#detail').val('');
        $('#author').val('');


    }
    function editData(id){
      
        $('.myid').show();
       
        $.ajax({
            type:'GET',
            dataType:'json',
           
           
            url:"create/"+id+"/edit",
            success:function(response)
            {
                $('#id').val(response.id);
                $('#name').val(response.name);
                $('#detail').val(response.detail);
                 $('#author').val(response.author);
                 $('#save').hide();
                 $('#update').show();

            }

        })

       
        
        

    }
    function deleteData(id)
    {
        $.ajax({
            type:'DELETE',
            dataType:'json',
           
            url:"create/"+id,
            success:function(reponse){
                viewData();
            }
                       
        })

    }
    </script>
</body>
</html>